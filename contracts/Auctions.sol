pragma solidity ^0.4.23;
contract Auction {

    // Data structure to hold details of the item
    event FinishAuction(bool finish);
    struct Item {
        uint itemId; // id of the item
        address[] addrTokens; // tokens bid in favor of the item
    }
    bool public finishAuction;
    struct Person {
        uint remainingTokens;
        uint personId;
        address addr;
    }
    mapping(address => Person) tokenDetails;

    Item[3] public items;
    address[3] public winners;
    address public beneficiary;
    uint bidderCount = 0;

    constructor() public payable {
        beneficiary = msg.sender;
        finishAuction = false;
        address[] memory emptyArray;
        for(uint i = 0; i < 3; i++) {
            items[i] = Item({itemId: i, addrTokens: emptyArray});
        }
    }

    function buyTokens() public payable {
        require(finishAuction == false, "Auction ended");
        require(msg.value >= 3 ether, "Not enoungh ether");
        Person storage bidder = tokenDetails[msg.sender];
        if(bidder.personId == 0){
            bidderCount++;
            bidder.personId = bidderCount;
            bidder.addr = msg.sender;
            bidder.remainingTokens = 0;
        }
        bidder.remainingTokens += uint(msg.value/3 ether);
        // tokenDetails[msg.sender] = bidder;
    }

    function bid(uint _itemId, uint _count) public payable{
        /*
            Bids tokens to a particular item.
            Arguments:
            _itemId -- uint, id of the item
            _count -- uint, count of tokens to bid for the item
        */
        require(finishAuction == false, "Auction ended");
        require(_itemId >= 0 && _itemId < 3, "Item id is invalid");
        require(tokenDetails[msg.sender].remainingTokens >= _count, "Not enoungh tokens");

        uint balance = tokenDetails[msg.sender].remainingTokens - _count;
        tokenDetails[msg.sender].remainingTokens = balance;
        
        Item storage bidItem = items[_itemId];
        for(uint i = 0; i < _count; i++) {
            bidItem.addrTokens.push(tokenDetails[msg.sender].addr);    
        }
    }

    modifier onlyOwner {
        require(msg.sender == beneficiary, "Only beneficiary can do this");
        _;
    }

    function revealWinners() public onlyOwner{
        /* 
            Iterate over all the items present in the auction.
            If at least on person has placed a bid, randomly select the winner 
        */
        require(finishAuction == false, "revealed winners");
        finishAuction = true;
        beneficiary.transfer(address(this).balance);
        for (uint id = 0; id < 3; id++) {
            Item storage currentItem = items[id];
            if(currentItem.addrTokens.length != 0){
                uint randomIndex = random() % currentItem.addrTokens.length; 
                winners[id] = currentItem.addrTokens[randomIndex];
            }
        }
        emit FinishAuction(true);
    }
    
    function getPersonDetails(address addr) public view returns(uint,uint,address){
        Person storage person = tokenDetails[addr];
        return (person.remainingTokens, person.personId, person.addr);
    }

    function random() private view returns (uint) {
        return uint(keccak256(abi.encodePacked(block.timestamp, block.difficulty)));
    } 
}