/*
init blockchain data node 1
geth --datadir ./chain_data/node1 init ./eth-config/genesis.json
run node 1
geth --datadir ./chain_data/node1 --networkid 5678 --rpc console 2>> logs/myEth.log
*/
// add an account
personal.importRawKey('6c1e1a69d73fcbb2260bf0d4f30b04c8eb041a7e9faef27174f7b7d84f48d8da', 'matkhauacc1')
personal.importRawKey('8B82B252540800D66BD21610CB082644E079E22DDB99A7A39BEF05D6F61E8E6C', 'matkhauacc3')
personal.importRawKey('63be0ca54e6c552215ad762fc92fceae72564d4f3615b3303c62f63c3a3b79d7', 'matkhauacc5')

// mining
miner.start(1)
