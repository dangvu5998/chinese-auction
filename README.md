# Chinese auction triển khai bằng blockchain

## Thiết lập private network sử dụng ethereum
1. Khởi tạo database
* Khởi tạo node 1
```
geth --datadir ./chain_data/node1 init ./eth-config/genesis.json
```
Làm tương tự với các node khác

2. Khởi động mạng ethereum
```
geth --datadir ./chain_data/node1 --networkid 5678 --rpc console 2>> logs/eth-node1.log
```
Command này sẽ khởi động mạng ethereum với database lưu trong thư mục `./chain_data/node1` và có networkid là 5678

Import tài khoản `7Ee19B481E99D350531296564FDC9d818e5FF2F5` vào node hiện tại
```
personal.importRawKey('6ad5477964bc86bee1a459efc861ef46ad42d3df03d1c02ac3c6a5a4a284952d', 'matkhauacc1')
```
Thực hiện mining:
```
miner.start(1)
```

3. Triển khai smart contract
Build solidity code:
```
truffle compile
```
Trên console ethereum đang chạy, import tài khoản `7Ee57222fc91Ef3F4cF8B3322420AdB8f379886e`:
```
personal.importRawKey('171038cd5cff22839db386ef8d84bb59423879a38d3463fa7f15c9a5f010a815', 'matkhauacc6')
```
Unlock tài khoản này:
```
personal.unlockAccount("0x7Ee57222fc91Ef3F4cF8B3322420AdB8f379886e")
```
Triển khai smart contract sử dụng tài khoản này:
```
truffle migrate --reset --network my_network
```
4. Chạy client kết nối đến smart contract này:
```
cd client
npm run start
```
Sử dụng browser `localhost:3000` vào web để xem giao diện

5. Cài đặt metamask để kết nối với webapp và thực hiện giao dịch trên trình duyệt tại [https://metamask.io/](https://metamask.io/)
