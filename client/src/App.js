import React, { Component } from 'react';
import contract from 'truffle-contract';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AuctionArtifact from './contracts/Auction.json';
import ItemList from './components/ItemList';
import BuyTokens from './components/BuyTokens';

class App extends Component {
  state = {
    auctionContract: {
      loading: true,
      auctionInstance: null,
    },
    account: null,
    beneficiary: null,
    finishAuction: null,
    remainingTokens: null
  }
  updatePersonDetails() {
    if(this.state.account && this.state.auctionContract.auctionInstance) {
      this.state.auctionContract.auctionInstance.getPersonDetails.call(this.state.account).then(details => {
        let remainingTokens = details[0].toNumber();
        this.setState({...this.state, remainingTokens});
      }).catch(e => {
        console.log(e)
      })
    }
  }
  revealWinner() {
    const {account, auctionContract} = this.state;
    if(auctionContract.loading) return;
    let {auctionInstance} = auctionContract
    window.instance = auctionInstance
    if(account && auctionInstance) {
      auctionInstance.revealWinners({from: account}).then(res => {
        console.log(res);
      }).catch(e => {
        console.log(e);
      })
    }
  }
  componentDidMount() {
    let {web3, Web3, ethereum} = window;
    let web3Provider;
    if(typeof web3 !== 'undefined') {
      web3Provider = web3.currentProvider;
    } else {
      web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(web3Provider);
    }
    ethereum.enable().then(accounts => {
      this.setState({account: accounts[0]})
    });
    let auctionContract = contract(AuctionArtifact);
    auctionContract.setProvider(web3Provider);
    auctionContract.deployed().then(auctionInstance => {
      window.auctionInstance = auctionInstance;
      auctionInstance.FinishAuction().watch((err, res) => {
        if(err) {
          console.log(err);
          return;
        }
        this.setState({...this.state, finishAuction: res.args.finish})
      })
      this.setState({...this.state, auctionContract: {loading: false, auctionInstance}});
      return auctionInstance;
    }).then(auctionInstance => {
      return Promise.all([auctionInstance.beneficiary.call(), auctionInstance.finishAuction.call()]);
    }).then(([beneficiary, finishAuction]) => {
      this.setState({...this.state, beneficiary, finishAuction})
      this.updatePersonDetails();
    }).catch(err => {
      console.log(err)
    });
  }
  render() {
    let style = {
      margin: "auto",
      maxWidth: "960px",
      paddingRight: "10px",
      paddingLeft: "10px"
    }
    if(this.state.auctionContract.loading) return <div>loading</div>;
    return (
      <div style={style}>
        <Typography component="h2" variant="h4" gutterBottom>
          Đấu giá Trung Hoa
        </Typography>
        {this.state.account && 
        <Typography variant="h5" gutterBottom>
          Tài khoản của bạn là: {this.state.account}
        </Typography>
        }
        {this.state.beneficiary && 
        <Typography variant="h5" gutterBottom>
          Chủ tài sản đấu giá là: {this.state.beneficiary}
        </Typography>
        }
        
        <div>
          <Typography variant="h5" gutterBottom>
            Vật đấu giá
          </Typography>
          <ItemList
            updatePersonDetails={this.updatePersonDetails.bind(this)}
            account={this.state.account}
            finishAuction={this.state.finishAuction} 
            remainingTokens={this.state.remainingTokens}
            auctionInstance={this.state.auctionContract.auctionInstance}
          />
        </div>
        {this.state.remainingTokens !== null &&
        <Typography variant="h6">
          Tokens: {this.state.remainingTokens}
          <br/>
          Price of tokens: 3 ethereum
        </Typography>
        }
        {this.state.account && this.state.beneficiary && this.state.finishAuction === false &&
        this.state.account !== this.state.beneficiary &&
        <BuyTokens 
          auctionInstance={this.state.auctionContract.auctionInstance} 
          account={this.state.account}
          updatePersonDetails={this.updatePersonDetails.bind(this)}
        />
        }
        {this.state.account && this.state.beneficiary && 
        this.state.finishAuction === false && this.state.account === this.state.beneficiary &&
        <Button color="secondary" onClick={() => {this.revealWinner()}}>Kết thúc đấu giá</Button>
        }
      </div>
    );
  }
}

export default App;
