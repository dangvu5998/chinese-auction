import React, {Component} from 'react';

export default function Item(props) {
  return (
    <span>
      <img src={props.imageSrc} alt={props.description} width={300}/>
    </span>
  )
}