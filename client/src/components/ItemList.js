import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import Modal from '@material-ui/core/Modal';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 800,
    height: 250,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
});

class ItemList extends Component {
  state = {
    items: [
      {
        id: 0,
        img: "images/pure-diamond.jpg",
        title: 'Pure diamond',
        owner: 'Not determined',
      },
      {
        id: 1,
        img: "images/red-diamond.jpg",
        title: 'Red diamond',
        owner: 'Not determined',
      },
      {
        id: 2,
        img: "images/green-diamond.jpg",
        title: 'Green diamond',
        owner: 'Not determined',
      }
    ],
    itemWillBid: 0,
    tokensWillBid: 0,
    modalOpen: false,
    gotWinners: false
  }
  handleTokensField(e) {
    let tokens = Number(e.target.value)
    if(Number.isInteger(tokens) && tokens >= 0 && tokens <= this.props.remainingTokens) {
      this.setState({...this.state, tokensWillBid: tokens});
    }
  }
  bidItem() {
    this.props.auctionInstance.bid(this.state.itemWillBid, this.state.tokensWillBid,
      {from: this.props.account}).then(res => {
        this.props.updatePersonDetails();
        this.setState({modalOpen: false});
        console.log(res);
      }).catch(e => {
        this.setState({modalOpen: false});
        console.log(e);
      })
  }
  handleClickItem(id) {
    this.setState({...this.state, itemWillBid: id, modalOpen: true})
  }
  componentDidUpdate() {
    if(this.props.finishAuction && !this.state.gotWinners) {
      this.setState({...this.state, gotWinners: true})
      for(let i = 0; i < 3; i++)
        this.props.auctionInstance.winners(i).then(winner => {
          this.setState({...this.state, items: this.state.items.map(item => {
            if(item.id === i) {
              return {...item, owner: winner}
            }
            return item
          })})
        })
    }
  }
  render() {
    const { classes, finishAuction } = this.props;
    let modalStyle = {
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      position: 'absolute',
      width: 300,
      backgroundColor: 'white',
      boxShadow: 3,
      padding: 20,
    }
    let textFieldStyle ={
      marginTop: 20,
      marginLeft: 20,
      marginBottom: 20,
      width: 200,
    }
    return (
      <div className={classes.root}>
        <GridList cellHeight={180} className={classes.gridList} cols={3}>
          {this.state.items.map(item => (
            <GridListTile key={item.img}>
              <img src={item.img} alt={item.title} />
              <GridListTileBar
                title={item.title}
                subtitle={<span title={item.owner}>Owner: {item.owner}</span>}
                actionIcon={
                  !finishAuction &&
                  <IconButton className={classes.icon}>
                    <ShoppingCart onClick={() => this.handleClickItem(item.id)}/>
                  </IconButton>
                }
              />
            </GridListTile>
          ))}
        </GridList>
        {finishAuction && 
        <div>
          <Typography variant="h6">Winner 0: {this.state.items[0].owner}</Typography>
          <Typography variant="h6">Winner 1: {this.state.items[1].owner}</Typography>
          <Typography variant="h6">Winner 2: {this.state.items[2].owner}</Typography>
        </div>
        }
        <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.modalOpen}
            onClose={() => {this.setState({...this.state, modalOpen: false})}}
            >
            <div style={modalStyle}>
              <Typography variant="h5">Đặt hàng thứ {this.state.itemWillBid}</Typography>
              <TextField
                style={textFieldStyle}
                label="Tokens" 
                value={this.state.tokensWillBid}
                type="number"
                onChange={this.handleTokensField.bind(this)}
              />
              <br/>
              <div>Max tokens: {this.props.remainingTokens}</div>
              <Button color="primary" onClick={() => this.bidItem()}>Đặt hàng</Button>
            </div>
          </Modal>
      </div>
    );
  }
}

ItemList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ItemList);