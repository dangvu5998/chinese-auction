import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

export default class BuyTokens extends Component{
    state = {
      modalOpen: false,
      tokensWillBuy: ''
    }
    buyTokens() {
      let {tokensWillBuy} = this.state;
      tokensWillBuy = Number(tokensWillBuy);
      window.auctionInstance = this.props.auctionInstance;
      if(!tokensWillBuy) return;
      this.props.auctionInstance.buyTokens({
        from: this.props.account, 
        value: window.web3.toWei(tokensWillBuy*3)
      }).then(res => {
        console.log(res)
        this.props.updatePersonDetails()
        this.setState({modalOpen: false});
      }).catch(err => {
        console.log(err)
        this.setState({modalOpen: false});
      });
    }
    render() {
      let textField ={
        marginTop: 20,
        marginLeft: 20,
        marginBottom: 20,
        width: 200,
      }
      let modalStyle = {
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        position: 'absolute',
        width: 300,
        backgroundColor: 'white',
        boxShadow: 3,
        padding: 20,
      }
      return (
        <div>
          <Button 
            color="primary" 
            onClick={() => {this.setState({...this.state, modalOpen: true})}}>
            Mua token
          </Button>
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.modalOpen}
            onClose={() => {this.setState({modalOpen: false})}}
            >
            <div style={modalStyle}>
              <Typography variant="h5">Mua tokens</Typography>
              <TextField
                style={textField}
                label="Tokens" 
                value={this.state.tokensWillBuy}
                type="number"
                onChange={e => {this.setState({...this.state, tokensWillBuy: e.target.value})}}
              />
              <Typography variant="h6">
                Thành tiền: {this.state.tokensWillBuy ?((this.state.tokensWillBuy * 3).toFixed(2)): 0}
              </Typography>
              <br/>
              <Button color="primary" onClick={() => this.buyTokens()}>Mua tokens</Button>
            </div>
          </Modal>
        </div>
      )
    }
}